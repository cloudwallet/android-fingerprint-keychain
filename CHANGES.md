# Changes

## [Unreleased]

## 1.0.0 (2017-08-31)
- added KeyManager to control key store in android
- added FingerprintScanner to scan fingerprint and to handle callback
- added FingerprintAuthenticationDialogFragment for scanning ui
