/*
 * Blocko Inc.
 * __________________
 *
 * (C) 2017 Blocko Inc. Some rights reserved. <tech@blocko.io>
 *
 * This file is part of Coinstack OpenKeyChain Android Fingerprint Authentication.
 *
 * Coinstack OpenKeyChain Android Fingerprint Authentication under MIT License
 *
 * See LICENSE file for details
 */
package io.blocko.coinstack.keychain.fingerprint.android.activity;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.FragmentManager;
import android.support.v4.hardware.fingerprint.FingerprintManagerCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import java.io.UnsupportedEncodingException;

import javax.crypto.Mac;

import io.blocko.android_fingerprint_keychain.R;
import io.blocko.coinstack.keychain.fingerprint.android.FingerprintAuthenticationHandler;
import io.blocko.coinstack.keychain.fingerprint.android.FingerprintAuthenticationProcess;
import io.blocko.coinstack.keychain.fingerprint.android.FingerprintScannerStatus;
import io.blocko.coinstack.keychain.fingerprint.android.KeyManager;
import io.blocko.coinstack.keychain.fingerprint.android.dialog.FingerprintAuthenticationDialogFragment;
import io.blocko.coinstack.keychain.fingerprint.android.exception.KeyStoreException;
import io.blocko.coinstack.keychain.fingerprint.android.util.HexUtils;
import io.blocko.coinstack.keychain.fingerprint.android.util.MacFactory;
import io.realm.Realm;
import io.realm.RealmResults;

/**
 * Main activity containing key list and add button
 */
public class MainActivity extends AppCompatActivity {

    protected static final String TAG = "main_activity";

    protected Realm realm;

    @Override
    protected void onCreate( final Bundle savedInstanceState ) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_main );
        final Toolbar toolbar = (Toolbar) findViewById( R.id.toolbar );
        setSupportActionBar(toolbar);

        final FloatingActionButton fab = (FloatingActionButton) findViewById( R.id.fab );
        fab.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick( final View view ) {
                onAddClick( view );
            }
        } );

        Realm.init( this );
        realm = Realm.getDefaultInstance();
        refresh();
    }

    /**
     * Redraw main activity
     */
    protected void refresh() {
        Log.v( TAG, "Refresh..." );
        final RealmResults<Key> keys = realm.where( Key.class ).findAllSorted( "value" );
        final TableLayout table = (TableLayout) findViewById( R.id.key_table );
        table.removeAllViews();
        final LayoutInflater layoutInflater = getLayoutInflater();
        for ( final Key key : keys ) {
            Log.v( TAG, "Item: " + key );
            final TableRow row = (TableRow) layoutInflater.inflate( R.layout.key_row, null, false );
            final TextView textView = row.findViewById( R.id.row_text );
            textView.setText( key.getValue() );
            row.setOnClickListener( new View.OnClickListener() {
                @Override
                public void onClick( View view ) {
                    onAuthenticationClick( view );
                }
            } );
            table.addView( row );
        }
    }

    /**
     * Callback method on add button click
     *
     * @param view
     *          add button
     */
    public void onAddClick(final View view ) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle( getString( R.string.input_dialog_title ) );

        // Set up the input
        final EditText input = new EditText(this);
        // Specify the type of input expected; this, for example, sets the input as a password, and will mask the text
        input.setInputType( InputType.TYPE_CLASS_TEXT );
        builder.setView( input );

        // Set up the buttons
        final String okText = getResources().getString(R.string.input_dialog_ok );
        builder.setPositiveButton( okText, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                addItem( input.getText().toString() );
            }
        });

        final String cancelText = getResources().getString(R.string.input_dialog_cancel );
        builder.setNegativeButton( cancelText, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        } );

        builder.show();
    }

    /**
     * Add key item
     *
     * @param itemValue
     *          key identifier value for key to add
     */
    public void addItem( final String itemValue ) {
        Log.v( TAG, "Add item: " + itemValue );
        final Realm realm = Realm.getDefaultInstance();
        if ( 0 == realm.where( Key.class ).equalTo( "value", itemValue ).count() ) {
            Log.v( TAG, "Register new key" );
            final KeyManager keyManager = new KeyManager();
            try {
                keyManager.addNewKey( itemValue );
                realm.executeTransaction(new Realm.Transaction() {
                    @Override
                    public void execute( final Realm realm) {
                        if ( 0 == realm.where( Key.class ).equalTo( "value", itemValue ).count() ) {
                            realm.insert( new Key( itemValue ) );
                            refresh();
                        }
                    }
                } );
                //register( itemValue );
            } catch ( Throwable e ) {
                Log.e( TAG, "Unexpected exception", e );
                showMessage( "Error: " + e.getMessage() );
            }
        } else {
            Log.w( TAG, "Already exists" );
            showMessage( getString(R.string.item_duplicated) );
        }
    }

    /**
     * Callback method on item click
     *
     * @param rowView
     *          row item to be clicked
     */
    public void onAuthenticationClick( final View rowView ) {
        final TextView textView = rowView.findViewById( R.id.row_text );
        authenticate( textView.getText().toString() );
    }

    /**
     * Get privatekey after authentication
     *
     * @param keyId
     *          key identifier
     */
    public void authenticate( final String keyId ) {
        try {
            MacFactory macFactory = new MacFactory();
            macFactory.setKeyManager( new KeyManager() );
            final Mac mac = macFactory.create( keyId );
            if ( null == mac ) {
                showMessage( "No key for " + keyId );
                return ;
            }
            final FingerprintAuthenticationProcess process = new FingerprintAuthenticationProcess();
            final FingerprintAuthenticationCallback handler = new FingerprintAuthenticationCallback() {
                @Override
                public void onSuccess( final FingerprintAuthenticationProcess process ) {
                    super.onSuccess( process );
                    try {
                        final String privatekey = HexUtils.hexFrom( mac.doFinal( keyId.getBytes( "UTF-8" ) ) );
                        showMessage( "Success: " + privatekey.substring( 0, 8 ) + "..." );
                    } catch ( UnsupportedEncodingException e ) {
                        throw new IllegalStateException( e );
                    }
                }
            };

            handler.setFragmentManager( getSupportFragmentManager() );
            process.setHandler( handler );
            process.setCryptoObject( new FingerprintManagerCompat.CryptoObject( mac ) );
            process.initialize( this );
            final FingerprintScannerStatus status = process.getScannerStatus();
            if ( status.isAvailable() ) {
                process.start();
            } else {
                showMessage( "Error: fingerprint scanner not available" );
            }
        } catch( final Throwable exception ) {
            showMessage( "Error: " + exception.getMessage() );
        }
    }

    /**
     * Show toast message on main activity
     *
     * @param message
     *          text to show
     */
    public void showMessage( final String message ) {
        Toast.makeText( this, message, Toast.LENGTH_LONG ).show();
    }
}
