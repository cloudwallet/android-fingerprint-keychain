/*
 * Blocko Inc.
 * __________________
 *
 * (C) 2017 Blocko Inc. Some rights reserved. <tech@blocko.io>
 *
 * This file is part of Coinstack OpenKeyChain Android Fingerprint Authentication.
 *
 * Coinstack OpenKeyChain Android Fingerprint Authentication under MIT License
 *
 * See LICENSE file for details
 */
package io.blocko.coinstack.keychain.fingerprint.android.activity;

import io.realm.RealmObject;
import lombok.Getter;
import lombok.Setter;

/**
 * Model for key identifier
 */
public class Key extends RealmObject {
    @Getter
    @Setter
    protected String value;

    public Key() {
    }

    public Key( final String value ) {
        this.value = value;
    }
}
