/*
 * Blocko Inc.
 * __________________
 *
 * (C) 2017 Blocko Inc. Some rights reserved. <tech@blocko.io>
 *
 * This file is part of Coinstack OpenKeyChain Android Fingerprint Authentication.
 *
 * Coinstack OpenKeyChain Android Fingerprint Authentication under MIT License
 *
 * See LICENSE file for details
 */
package io.blocko.coinstack.keychain.fingerprint.android.activity;

import android.content.Context;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.widget.Toast;

import java.nio.charset.Charset;

import io.blocko.android_fingerprint_keychain.R;
import io.blocko.coinstack.keychain.fingerprint.android.FingerprintAuthenticationHandler;
import io.blocko.coinstack.keychain.fingerprint.android.FingerprintAuthenticationProcess;
import io.blocko.coinstack.keychain.fingerprint.android.dialog.FingerprintAuthenticationDialogFragment;
import io.blocko.coinstack.keychain.fingerprint.android.dialog.FingerprintAuthenticationDialogFragmentListener;
import io.blocko.coinstack.keychain.fingerprint.android.exception.FingerprintLockoutException;
import lombok.Setter;

/**
 * Callback to retry 3 times
 */
public class FingerprintAuthenticationCallback implements FingerprintAuthenticationHandler, FingerprintAuthenticationDialogFragmentListener {
    protected static final String TAG = "main_callback";

    @Setter
    protected FragmentManager fragmentManager;

    protected FingerprintAuthenticationDialogFragment dialogFragment;

    @Setter
    protected FingerprintAuthenticationProcess authenticationProcess;

    @Setter
    protected int retry = 3;

    protected int iRetry = 0;

    @Override
    public void onStart( final FingerprintAuthenticationProcess process ) {
        Log.v( TAG, "Starting the process..." );
        this.iRetry = 0;
        this.authenticationProcess = process;
        dialogFragment = new FingerprintAuthenticationDialogFragment();
        dialogFragment.addListener( this );
        fragmentManager.beginTransaction().addToBackStack( "fingerprint_dialog_fragment" ).add( dialogFragment, "fingerprint_dialog_fragment" ).commit();
    }

    @Override
    public void onEnd( final FingerprintAuthenticationProcess process ) {
        this.authenticationProcess = null;
        if ( !this.dialogFragment.isDismissed() ) {
            this.dialogFragment.dismiss();
        }
        Log.v( TAG, "Ended the process..." );
    }

    @Override
    public void onSuccess( final FingerprintAuthenticationProcess process ) {
        Log.v( TAG, "Success to authenticate..." );
    }

    @Override
    public void onFailure( final FingerprintAuthenticationProcess process, final CharSequence help ) {
        Log.v( TAG, "Fail to authenticate..." + (help==null?"":help) );
        ++iRetry;
        Log.d( TAG, iRetry + " retries" );
        if ( retry <= iRetry ) {
            process.stop();
            Toast.makeText( dialogFragment.getActivity(), "Fail to authenticate", Toast.LENGTH_LONG ).show();
        } else {
            dialogFragment.showError( ((null==help)?"Failed to authenticate":help.toString()) );
        }
    }

    @Override
    public void onError( final FingerprintAuthenticationProcess process, final Throwable exception ) {
        Log.v( TAG, "Error: ", exception );
        if ( exception instanceof FingerprintLockoutException ) {
            Toast.makeText( dialogFragment.getActivity(), "Fingerprint locked out", Toast.LENGTH_LONG ).show();
        }
    }

    @Override
    public void onCreate( final FingerprintAuthenticationDialogFragment fragment ) {
    }

    @Override
    public void onDismiss( final FingerprintAuthenticationDialogFragment fragment ) {
        Log.v( TAG, "Fragment is dismissed" );
        if ( null != authenticationProcess ) {
            authenticationProcess.stop();
            Toast.makeText( dialogFragment.getActivity(), "Authentication cancelled", Toast.LENGTH_LONG ).show();
        }
    }
}
