# Overview
Provide fingerprint authentication

# License
Distributed under the MIT License

See LICENSE file or link

https://opensource.org/licenses/MIT

# Build
gradlew build

# Usage
## Download Android Archive
https://blocko.blob.core.windows.net/coinstack/coinstack-fingerprint-1.0.0.aar

## Import Android Archive
See https://developer.android.com/studio/projects/android-library.html

# Reference
https://blocko.gitbooks.io/coinstack-openkeychain/content/
