package io.blocko.coinstack.keychain.fingerprint.android;

import junit.framework.TestSuite;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

import io.blocko.coinstack.keychain.fingerprint.android.util.HexUtils;
import io.blocko.coinstack.keychain.fingerprint.android.util.HexUtilsTest;

/**
 * Created by blocko on 2017. 8. 28..
 */
@RunWith(Suite.class )
@Suite.SuiteClasses( { FingerprintScannerTest.class, HexUtilsTest.class } )
public class AllTests {
}
