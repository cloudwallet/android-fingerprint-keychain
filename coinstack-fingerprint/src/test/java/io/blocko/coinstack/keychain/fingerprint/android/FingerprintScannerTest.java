package io.blocko.coinstack.keychain.fingerprint.android;

import android.content.Context;
import android.support.v4.hardware.fingerprint.FingerprintManagerCompat;
import android.test.mock.MockContext;
import android.util.Log;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

import javax.crypto.Mac;

import io.blocko.coinstack.keychain.fingerprint.android.exception.FingerprintScanException;
import io.blocko.coinstack.keychain.fingerprint.android.exception.NoFingerprintEnrolledException;
import io.blocko.coinstack.keychain.fingerprint.android.exception.NoFingerprintPermissionException;
import io.blocko.coinstack.keychain.fingerprint.android.exception.NoFingerprintScannerException;

import static org.powermock.api.mockito.PowerMockito.mock;
import static org.powermock.api.mockito.PowerMockito.mockStatic;
import static org.powermock.api.mockito.PowerMockito.when;
import static org.powermock.api.mockito.PowerMockito.whenNew;
import static org.powermock.api.support.membermodification.MemberMatcher.method;
import static org.powermock.api.support.membermodification.MemberModifier.replace;
import static android.support.v4.hardware.fingerprint.FingerprintManagerCompat.AuthenticationCallback;

/**
 * Created by blocko on 2017. 8. 28..
 */
@RunWith(PowerMockRunner.class)
public class FingerprintScannerTest {

    @Test( expected = NoFingerprintScannerException.class )
    @PrepareForTest( Log.class )
    public void shouldThrowExceptionOnNoSensor() throws Exception {
        mockStatic( Log.class );
        final Context context = new MockContext();
        final FingerprintScanner scanner = new FingerprintScanner( context );
        scanner.assertAvailable();
    }

    @Test( expected = NoFingerprintEnrolledException.class )
    @PrepareForTest( { FingerprintManagerCompat.class, Log.class } )
    public void shouldThrowExceptionOnNoFingerprintEnrolled() throws Exception {
        mockStatic( Log.class );
        final Context context = new MockContext();
        final FingerprintManagerCompat fm = mock( FingerprintManagerCompat.class );
        whenNew(FingerprintManagerCompat.class ).withParameterTypes( Context.class ).withArguments( context ).thenReturn( fm );
        final FingerprintScanner scanner = new FingerprintScanner( context );
        when( fm.isHardwareDetected() ).thenReturn( true );
        scanner.assertAvailable();
    }

    @Test( expected = NoFingerprintPermissionException.class )
    @PrepareForTest( { FingerprintManagerCompat.class, Log.class } )
    public void shouldThrowExceptionOnNoPermission() throws Exception {
        mockStatic( Log.class );
        final Context context = new MockContext();
        final FingerprintManagerCompat fm = mock( FingerprintManagerCompat.class );
        whenNew(FingerprintManagerCompat.class ).withParameterTypes( Context.class ).withArguments( context ).thenReturn( fm );
        final FingerprintScanner scanner = new FingerprintScanner( context );
        when( fm.isHardwareDetected() ).thenThrow( new SecurityException( "Must have android.permission.USE_FINGERPRINT permission.: Neither user 10144 nor current process has android.permission.USE_FINGERPRINT." ) );
        scanner.assertAvailable();
    }

    @Test
    @PrepareForTest( { FingerprintManagerCompat.class, Log.class } )
    public void testStart() throws Exception {
        mockStatic( Log.class );
        final AuthenticationCallback callback = mock(AuthenticationCallback.class );
        final Context context = new MockContext();
        final FingerprintManagerCompat fm = mock( FingerprintManagerCompat.class );
        whenNew(FingerprintManagerCompat.class ).withParameterTypes( Context.class ).withArguments( context ).thenReturn( fm );
        final FingerprintScanner scanner = new FingerprintScanner( context );
        when( fm.isHardwareDetected() ).thenReturn( true );
        when( fm.hasEnrolledFingerprints() ).thenReturn( true );
        final Mac mac = mock( Mac.class );
        scanner.setAuthenticationCallback( callback );
        scanner.setCryptoObject( new FingerprintManagerCompat.CryptoObject( mac ) );
        scanner.start();
    }

}
