package io.blocko.coinstack.keychain.fingerprint.android.util;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created by blocko on 2017. 8. 29..
 */

public class HexUtilsTest {
    @Test
    public void testHexFrom() {
        assertEquals( "48656c6c6f", HexUtils.hexFrom( "Hello".getBytes() ) );
        assertEquals( "09", HexUtils.hexFrom( new byte[] { 0x09 } ) );
    }
}
