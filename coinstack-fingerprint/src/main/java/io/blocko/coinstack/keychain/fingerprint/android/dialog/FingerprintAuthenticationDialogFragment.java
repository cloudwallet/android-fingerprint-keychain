/*
 * Blocko Inc.
 * __________________
 *
 * (C) 2017 Blocko Inc. Some rights reserved. <tech@blocko.io>
 *
 * This file is part of Coinstack OpenKeyChain Android Fingerprint Authentication.
 *
 * Coinstack OpenKeyChain Android Fingerprint Authentication under MIT License
 *
 * See LICENSE file for details
 */
package io.blocko.coinstack.keychain.fingerprint.android.dialog;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import io.blocko.coinstack.keychain.fingerprint.android.R;
import lombok.Getter;

/**
 * built-in {@link android.app.DialogFragment} in scanning fingerprint
 */
public class FingerprintAuthenticationDialogFragment extends DialogFragment {
    protected static final long ERROR_TIMEOUT_MILLIS = 1600;

    protected final List<FingerprintAuthenticationDialogFragmentListener> listeners = new ArrayList<>();

    protected ImageView icon;

    protected TextView text;

    @Getter
    protected boolean dismissed = false;

    /**
     * Add listener
     *
     * @param listener
     *          listener to listen to fragment
     */
    public void addListener( final FingerprintAuthenticationDialogFragmentListener listener ) {
        this.listeners.add( listener );
    }

    @Override
    public void onCreate( final Bundle savedInstanceState ) {
        super.onCreate( savedInstanceState );
        setRetainInstance( true );
        setStyle( DialogFragment.STYLE_NO_TITLE, android.R.style.Theme_Material_Light_Dialog );
        for ( final FingerprintAuthenticationDialogFragmentListener listener : listeners ) {
            listener.onCreate( this );
        }

    }

    @Nullable
    @Override
    public View onCreateView( final LayoutInflater inflater, @Nullable final ViewGroup container, @Nullable final Bundle savedInstanceState ) {
        final View view = inflater.inflate( R.layout.fragment_fingerprint_authentication, container );
        final TextView titleTextView = view.findViewById( R.id.fingerprint_title );
        text = view.findViewById( R.id.fingerprint_status );
        icon = view.findViewById( R.id.fingerprint_icon );
        final Button cancelButton = view.findViewById( R.id.cancel_button );
        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onCancelClick( view );
            }
        });
        final CharSequence titleText = titleTextView.getText();
        getDialog().setTitle( titleText );
        getDialog().setCanceledOnTouchOutside( false );
        setCancelable( false );
        return view;
    }

    public void onCancelClick(final View view ) {
        dismiss();
    }

    @Override
    synchronized  public void onDismiss(DialogInterface dialog) {
        this.dismissed = true;
        for ( final FingerprintAuthenticationDialogFragmentListener listener : listeners ) {
            listener.onDismiss( this );
        }
        super.onDismiss(dialog);
    }

    public void showError( final String message ) {
        if ( this.isDetached() ) {
            return ;
        }
        text.setText( message );
        text.setTextColor( getResources().getColor( R.color.warning_color, null ) );
        icon.setImageResource( R.drawable.ic_fp_fail_40px );
        text.postDelayed(new Runnable() {
            @Override
            public void run() {
                resetMessage();
            }
        }, ERROR_TIMEOUT_MILLIS );
    }

    public void showMessage( final String message ) {
        if ( this.isDetached() ) {
            return ;
        }
        text.setText( message );
        text.setTextColor( getResources().getColor( R.color.hint_color, null ) );
        icon.setImageResource( R.drawable.ic_fp_40px );
    }

    synchronized public void resetMessage() {
        if ( this.dismissed ) {
            return ;
        }
        showMessage( getResources().getString( R.string.authentication_fragment_hint ) );
    }
}
