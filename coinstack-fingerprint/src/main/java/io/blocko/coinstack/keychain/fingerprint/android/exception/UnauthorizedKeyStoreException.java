/*
 * Blocko Inc.
 * __________________
 *
 * (C) 2017 Blocko Inc. Some rights reserved. <tech@blocko.io>
 *
 * This file is part of Coinstack OpenKeyChain Android Fingerprint Authentication.
 *
 * Coinstack OpenKeyChain Android Fingerprint Authentication under MIT License
 *
 * See LICENSE file for details
 */
package io.blocko.coinstack.keychain.fingerprint.android.exception;

/**
 * Exception in unauthorized accessing
 */
public class UnauthorizedKeyStoreException extends KeyStoreException {
    public UnauthorizedKeyStoreException( Throwable cause ) {
        super( "Unahutorized to access keystore", cause );
    }
}
