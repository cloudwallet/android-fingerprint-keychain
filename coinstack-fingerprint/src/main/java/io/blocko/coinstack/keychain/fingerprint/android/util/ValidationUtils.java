/*
 * Blocko Inc.
 * __________________
 *
 * (C) 2017 Blocko Inc. Some rights reserved. <tech@blocko.io>
 *
 * This file is part of Coinstack OpenKeyChain Android Fingerprint Authentication.
 *
 * Coinstack OpenKeyChain Android Fingerprint Authentication under MIT License
 *
 * See LICENSE file for details
 */
package io.blocko.coinstack.keychain.fingerprint.android.util;

/**
 * Utilities for assertion
 */
public class ValidationUtils {
    public static void assertTrue( final boolean exp, final RuntimeException re ) {
        if ( exp ) {
            return ;
        }

        throw re;
    }

    public static void assertFalse( final boolean exp, final RuntimeException re ) {
        assertTrue( !exp, re );
    }

    public static void assertNull( final Object obj, final RuntimeException re ) {
        assertTrue( null == obj, re );
    }

    public static void assertNotNull( final Object obj, final RuntimeException re ) {
        assertTrue( null != obj, re );
    }

}
