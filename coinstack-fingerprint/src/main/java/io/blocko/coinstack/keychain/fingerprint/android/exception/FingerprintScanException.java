/*
 * Blocko Inc.
 * __________________
 *
 * (C) 2017 Blocko Inc. Some rights reserved. <tech@blocko.io>
 *
 * This file is part of Coinstack OpenKeyChain Android Fingerprint Authentication.
 *
 * Coinstack OpenKeyChain Android Fingerprint Authentication under MIT License
 *
 * See LICENSE file for details
 */
package io.blocko.coinstack.keychain.fingerprint.android.exception;

import lombok.Getter;

/**
 * Parent exception in scanning fingerprint
 */
public class FingerprintScanException extends Exception {

    @Getter
    protected final int errorCode;

    public FingerprintScanException() {
        errorCode = -1;
    }

    public FingerprintScanException( Throwable cause ) {
        super( cause );
        errorCode = 0;
    }

    public FingerprintScanException( int errorCode ) {
        this.errorCode = errorCode;
    }
}
