/*
 * Blocko Inc.
 * __________________
 *
 * (C) 2017 Blocko Inc. Some rights reserved. <tech@blocko.io>
 *
 * This file is part of Coinstack OpenKeyChain Android Fingerprint Authentication.
 *
 * Coinstack OpenKeyChain Android Fingerprint Authentication under MIT License
 *
 * See LICENSE file for details
 */
package io.blocko.coinstack.keychain.fingerprint.android.exception;

/**
 * Exception when being denied to access keystore.
 */
public class KeyAccessException extends  KeyStoreException {
    public KeyAccessException(final Throwable cause ) {
        super( "Failed to access keystore", cause );
    }
}
