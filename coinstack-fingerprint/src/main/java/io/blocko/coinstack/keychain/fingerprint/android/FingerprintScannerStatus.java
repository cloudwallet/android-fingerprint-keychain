/*
 * Blocko Inc.
 * __________________
 *
 * (C) 2017 Blocko Inc. Some rights reserved. <tech@blocko.io>
 *
 * This file is part of Coinstack OpenKeyChain Android Fingerprint Authentication.
 *
 * Coinstack OpenKeyChain Android Fingerprint Authentication under MIT License
 *
 * See LICENSE file for details
 */
package io.blocko.coinstack.keychain.fingerprint.android;

import lombok.Getter;
import lombok.Setter;

/**
 * {@link FingerprintScanner}'s status
 */
public class FingerprintScannerStatus {

    @Getter
    protected final boolean hardwareDetected;

    protected final boolean enrolledFingerprints;

    public FingerprintScannerStatus( final boolean hardwareDetected, final boolean enrolledFingerprints) {
        this.hardwareDetected = hardwareDetected;
        this.enrolledFingerprints = enrolledFingerprints;
    }

    /**
     * Check if fingerprint scanning is available
     *
     * @return
     *      {@code true} if available
     * @see
     *      #isHardwareDetected()
     *      #hasEnrolledFingerPrints()
     */
    public boolean isAvailable() {
        return isHardwareDetected() && hasEnrolledFingerPrints();
    }

    /**
     * Check if registered fingerprint exists
     *
     * @return
     *      {@code true} if exists
     */
    public boolean hasEnrolledFingerPrints() {
        return enrolledFingerprints;
    }
}
