package io.blocko.coinstack.keychain.fingerprint.android.exception;

/**
 * Created by blocko on 2017. 8. 25..
 */

public class KeyStoreLoadException extends  KeyStoreException {
    public KeyStoreLoadException( final Throwable cause ) {
        super( "Failed to load keystore", cause);
    }
}
