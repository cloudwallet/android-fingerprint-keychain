/*
 * Blocko Inc.
 * __________________
 *
 * (C) 2017 Blocko Inc. Some rights reserved. <tech@blocko.io>
 *
 * This file is part of Coinstack OpenKeyChain Android Fingerprint Authentication.
 *
 * Coinstack OpenKeyChain Android Fingerprint Authentication under MIT License
 *
 * See LICENSE file for details
 */
package io.blocko.coinstack.keychain.fingerprint.android;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.support.v4.hardware.fingerprint.FingerprintManagerCompat;
import android.support.v4.os.CancellationSignal;
import android.util.Log;

import java.util.Locale;

import io.blocko.coinstack.keychain.fingerprint.android.exception.FingerprintScanException;
import io.blocko.coinstack.keychain.fingerprint.android.exception.NoFingerprintEnrolledException;
import io.blocko.coinstack.keychain.fingerprint.android.exception.NoFingerprintPermissionException;
import io.blocko.coinstack.keychain.fingerprint.android.exception.NoFingerprintScannerException;
import lombok.Setter;

import static io.blocko.coinstack.keychain.fingerprint.android.util.ValidationUtils.assertNotNull;
import static io.blocko.coinstack.keychain.fingerprint.android.util.ValidationUtils.assertNull;

/**
 * Fingerprint scanner
 */
public class FingerprintScanner {
    protected static final String TAG = "fingerprint_scanner";
    protected FingerprintManagerCompat fm;

    protected CancellationSignal cancellationSignal;

    @Setter
    protected FingerprintManagerCompat.CryptoObject cryptoObject;

    @Setter
    protected FingerprintManagerCompat.AuthenticationCallback authenticationCallback;

    protected Locale locale;

    public FingerprintScanner( final Context context ) {
        this.fm = FingerprintManagerCompat.from( context );
    }

    public FingerprintScannerStatus getStatus() {
        return new FingerprintScannerStatus( this.fm.isHardwareDetected(), this.fm.hasEnrolledFingerprints() );
    }

    protected void assertAvailable() throws FingerprintScanException {
        try {
            if ( !fm.isHardwareDetected() ) {
                Log.v( TAG, "No fingerprint sensor" );
                throw new NoFingerprintScannerException();
            } else if ( !fm.hasEnrolledFingerprints() ) {
                Log.v( TAG, "No enrolled fingerprint" );
                throw new NoFingerprintEnrolledException();
            }
        } catch ( final SecurityException e ) {
            if ( e.getMessage().contains(Manifest.permission.USE_FINGERPRINT) ) {
                Log.v( TAG, "No " + Manifest.permission.USE_FINGERPRINT + " permission in AndroidManifest" );
                throw new NoFingerprintPermissionException( e );
            } else {
                Log.v( TAG, "Unexpected security exception" );
                throw e;
            }
        }
    }

    synchronized public boolean isStarted() {
        return !isStarted();
    }

    synchronized  public boolean isStopped() {
        return null == cancellationSignal;
    }

    /**
     * Start to scan fingerprint
     */
    synchronized public void start() throws FingerprintScanException {
        Log.v( TAG, "Starting to scan fingerprint..." );
        assertNull( cancellationSignal, new IllegalStateException( "Already started to scan" ) );
        assertNotNull( authenticationCallback, new IllegalStateException( "No callback for scan fingerprint" ) );
        assertAvailable();
        cancellationSignal = new CancellationSignal();
        fm.authenticate( this.cryptoObject, 0, cancellationSignal, authenticationCallback, null );
        Log.i( TAG, "Started to scan fingerprint" );
    }

    /**
     * Stop to scan fingerprint
     */
    synchronized public void stop() {
        assertNotNull( cancellationSignal, new IllegalStateException( "Already stopped to scan") );
        Log.v( TAG, "Stopping to scan fingerprint..." );
        this.cancellationSignal.cancel();
        this.cancellationSignal = null;
        Log.i( TAG, "Stopped to scan fingerprint" );
    }

}
