/*
 * Blocko Inc.
 * __________________
 *
 * (C) 2017 Blocko Inc. Some rights reserved. <tech@blocko.io>
 *
 * This file is part of Coinstack OpenKeyChain Android Fingerprint Authentication.
 *
 * Coinstack OpenKeyChain Android Fingerprint Authentication under MIT License
 *
 * See LICENSE file for details
 */
package io.blocko.coinstack.keychain.fingerprint.android;

import android.content.Context;

import io.blocko.coinstack.keychain.fingerprint.android.dialog.FingerprintAuthenticationDialogFragment;

/**
 * Callback handler for scanning fingerprint
 */
public interface FingerprintAuthenticationHandler {

    /**
     * Called when scan started
     *
     * @param process
     *          caller for event
     */
    void onStart( FingerprintAuthenticationProcess process );

    /**
     * Called when scan ended
     *
     * @param process
     *          caller for event
     */
    void onEnd( FingerprintAuthenticationProcess process );

    /**
     * Called when user authenticated
     *
     * @param process
     *          caller for event
     */
    void onSuccess( FingerprintAuthenticationProcess process );

    /**
     * Called when user authentication failed
     *
     * @param process
     *          caller for event
     * @param help
     *          help message for failure
     */
    void onFailure( FingerprintAuthenticationProcess process, CharSequence help );

    /**
     * Called when scan error
     *
     * @param process
     *          caller for event
     * @param exception
     *          cause for error
     */
    void onError( FingerprintAuthenticationProcess process, Throwable exception );
}
