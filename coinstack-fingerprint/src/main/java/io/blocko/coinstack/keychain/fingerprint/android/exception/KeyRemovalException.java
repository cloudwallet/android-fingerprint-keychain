/*
 * Blocko Inc.
 * __________________
 *
 * (C) 2017 Blocko Inc. Some rights reserved. <tech@blocko.io>
 *
 * This file is part of Coinstack OpenKeyChain Android Fingerprint Authentication.
 *
 * Coinstack OpenKeyChain Android Fingerprint Authentication under MIT License
 *
 * See LICENSE file for details
 */
package io.blocko.coinstack.keychain.fingerprint.android.exception;

/**
 * Exception in be failed to remove key from keystore.
 */
public class KeyRemovalException extends  KeyStoreException {
    public KeyRemovalException(final Throwable cause ) {
        super( "Failed to remove key", cause );
    }
}
