/*
 * Blocko Inc.
 * __________________
 *
 * (C) 2017 Blocko Inc. Some rights reserved. <tech@blocko.io>
 *
 * This file is part of Coinstack OpenKeyChain Android Fingerprint Authentication.
 *
 * Coinstack OpenKeyChain Android Fingerprint Authentication under MIT License
 *
 * See LICENSE file for details
 */
package io.blocko.coinstack.keychain.fingerprint.android;

import android.content.Context;
import static android.hardware.fingerprint.FingerprintManager.*;

import android.support.v4.hardware.fingerprint.FingerprintManagerCompat;
import android.util.Log;

import static android.support.v4.hardware.fingerprint.FingerprintManagerCompat.AuthenticationCallback;

import io.blocko.coinstack.keychain.fingerprint.android.dialog.FingerprintAuthenticationDialogFragment;
import io.blocko.coinstack.keychain.fingerprint.android.dialog.FingerprintAuthenticationDialogFragmentListener;
import io.blocko.coinstack.keychain.fingerprint.android.exception.FingerprintLockoutException;
import io.blocko.coinstack.keychain.fingerprint.android.exception.FingerprintScanException;
import io.blocko.coinstack.keychain.fingerprint.android.exception.NoFingerprintScannerException;
import lombok.Setter;

/**
 * Controller for {@link FingerprintScanner}
 */
public class FingerprintAuthenticationProcess extends AuthenticationCallback {
    protected static final String TAG = "fingerprint_process";

    protected static final int ST_STOP = 0;
    protected static final int ST_PROCESS = 1;
    protected static final int ST_CANCEL = 2;

    protected int state = ST_STOP;

    @Setter
    protected FingerprintManagerCompat.CryptoObject cryptoObject;

    @Setter
    protected FingerprintAuthenticationHandler handler;

    protected FingerprintScanner scanner;

    protected void assertState( int state ) {
        if ( this.state == state ) {
            return ;
        }
        throw new IllegalStateException();
    }

    /**
     * Initialize
     *
     * @param context
     *          {@link Context} for fingerprint scanner
     */
    synchronized  public void initialize( final Context context ) {
        scanner = new FingerprintScanner( context );
        if ( null != cryptoObject ) {
            scanner.setCryptoObject( cryptoObject );
        }
        scanner.setAuthenticationCallback( this );
    }

    /**
     * {@link #scanner}'s status
     * Return {@code null} if not initialized
     *
     * @return
     *      scanner's status
     */
    public FingerprintScannerStatus getScannerStatus() {
        if ( null == scanner ) {
            return null;
        }
        return scanner.getStatus();
    }

    synchronized public boolean isStarted() {
        return ST_PROCESS == state;
    }
    synchronized public boolean isStopped() {
        return !isStarted();
    }

    /**
     * Stop scanning
     */
    synchronized public void stop() {
        assertState( ST_PROCESS );
        scanner.stop();
        state = ST_STOP;
    }

    /**
     * Start scanning
     */
    synchronized public void start() {
        assertState( ST_STOP );
        handler.onStart( this );
        try {
            scanner.start();
            this.state = ST_PROCESS;
        } catch ( final Throwable exception ) {
            Log.e( TAG, "Unexpected error", exception );
            handler.onError( this, exception );
        }
    }

    @Override
    public void onAuthenticationError( final int errMsgId, final CharSequence errString ) {
        Log.v( TAG, "Error id: " + errMsgId + ", Message: " + errString );
        super.onAuthenticationError( errMsgId, errString );
        try {
            switch ( errMsgId ) {
                case FINGERPRINT_ERROR_CANCELED:
                    Log.d( TAG, "Ignore to cancel" );
                    break;
                case FINGERPRINT_ERROR_HW_UNAVAILABLE:
                    this.handler.onError( this, new NoFingerprintScannerException() );
                    break;
                case FINGERPRINT_ERROR_LOCKOUT:
                    this.handler.onError( this, new FingerprintLockoutException() );
                    break;
                default:
                    this.handler.onError( this, new FingerprintScanException( errMsgId ) );
                    break;
            }
        } finally {
            state = ST_STOP;
            handler.onEnd( this );
        }
    }

    @Override
    public void onAuthenticationSucceeded( final FingerprintManagerCompat.AuthenticationResult result) {
        Log.v( TAG, "Success to authentication" );
        super.onAuthenticationSucceeded(result);
        try {
            this.handler.onSuccess( this );
        } finally {
            this.handler.onEnd( this );
        }
    }

    @Override
    public void onAuthenticationFailed() {
        Log.v( TAG, "Fail to authentication" );
        super.onAuthenticationFailed();
        this.handler.onFailure( this, null );
    }

    @Override
    public void onAuthenticationHelp( final int helpMsgId, final CharSequence helpString ) {
        Log.v( TAG, "Help: " + helpMsgId + ", Message: " + helpString );
        super.onAuthenticationHelp( helpMsgId, helpString );
        this.handler.onFailure( this, helpString );
    }
}
