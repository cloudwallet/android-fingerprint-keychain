/*
 * Blocko Inc.
 * __________________
 *
 * (C) 2017 Blocko Inc. Some rights reserved. <tech@blocko.io>
 *
 * This file is part of Coinstack OpenKeyChain Android Fingerprint Authentication.
 *
 * Coinstack OpenKeyChain Android Fingerprint Authentication under MIT License
 *
 * See LICENSE file for details
 */
package io.blocko.coinstack.keychain.fingerprint.android.util;

import java.security.KeyStore;
import java.security.ProviderException;

import javax.crypto.Mac;
import javax.crypto.SecretKey;

import android.security.keystore.KeyPermanentlyInvalidatedException;
import android.security.keystore.KeyProperties;
import android.util.Log;

import io.blocko.coinstack.keychain.fingerprint.android.KeyManager;
import io.blocko.coinstack.keychain.fingerprint.android.exception.KeyAccessException;
import io.blocko.coinstack.keychain.fingerprint.android.exception.KeyStoreException;
import io.blocko.coinstack.keychain.fingerprint.android.exception.UnauthorizedKeyStoreException;
import lombok.Setter;

/**
 * Factory for {@link Mac}
 */
public class MacFactory {
    /**
     * algorithm to create mac
     *
     * default is {@link KeyProperties#KEY_ALGORITHM_HMAC_SHA256}
     */
    @Setter
    protected String macAlgorithm = KeyProperties.KEY_ALGORITHM_HMAC_SHA256;

    /**
     * key manager accessing key store for mac
     */
    @Setter
    protected KeyManager keyManager;

    /**
     * create {@link Mac} for key
     *
     * @param keyId
     *          identifier for access keystore
     * @return
     *          Mac to be created
     *
     * @throws KeyStoreException
     *          throw when failed access keystore
     */
    public Mac create( final String keyId ) throws KeyStoreException {
        try {
            final SecretKey key = keyManager.getKey( keyId );
            if ( key == null ) {
                return null;
            }

            final Mac mac = Mac.getInstance(macAlgorithm);
            mac.init(key);
            return mac;
        } catch (ProviderException e) {
            if (e.getCause().getMessage().equals("Key user not authenticated")) {
                throw new UnauthorizedKeyStoreException( e.getCause() );
            } else {
                throw new KeyAccessException( e.getCause() );
            }
        } catch ( final KeyPermanentlyInvalidatedException e ) {
            throw new KeyAccessException( e );
        } catch ( final Throwable unhandled ) {
            unhandled.printStackTrace();
            throw new KeyStoreException( "Failed to access keystore", (null==unhandled.getCause())?unhandled:unhandled.getCause() );
        }
    }
}
