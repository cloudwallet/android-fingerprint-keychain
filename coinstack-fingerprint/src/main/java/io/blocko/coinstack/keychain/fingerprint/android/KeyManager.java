/*
 * Blocko Inc.
 * __________________
 *
 * (C) 2017 Blocko Inc. Some rights reserved. <tech@blocko.io>
 *
 * This file is part of Coinstack OpenKeyChain Android Fingerprint Authentication.
 *
 * Coinstack OpenKeyChain Android Fingerprint Authentication under MIT License
 *
 * See LICENSE file for details
 */
package io.blocko.coinstack.keychain.fingerprint.android;

import android.security.keystore.KeyGenParameterSpec;
import android.security.keystore.KeyProperties;

import java.io.IOException;
import java.security.KeyStore;

import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;

import io.blocko.coinstack.keychain.fingerprint.android.exception.KeyAccessException;
import io.blocko.coinstack.keychain.fingerprint.android.exception.KeyGenerationException;
import io.blocko.coinstack.keychain.fingerprint.android.exception.KeyRemovalException;
import io.blocko.coinstack.keychain.fingerprint.android.exception.KeyStoreException;
import io.blocko.coinstack.keychain.fingerprint.android.exception.KeyStoreLoadException;
import lombok.Getter;
import lombok.Setter;

/**
 * Key manager for keystore
 */
public class KeyManager {

    @Getter
    @Setter
    protected String keystoreProvider = "AndroidKeyStore";
    @Getter
    @Setter
    protected String keyGenerationAlgorithm = KeyProperties.KEY_ALGORITHM_HMAC_SHA256;

    protected KeyStore keyStore;

    synchronized  protected KeyStore getKeyStore() throws KeyStoreLoadException {
        if ( null != keyStore ) {
            return keyStore;
        }
        try {
            keyStore = KeyStore.getInstance( keystoreProvider );
            keyStore.load( null );
            return keyStore;
        } catch ( final Exception e ) {
            throw new KeyStoreLoadException( e );
        }
    }

    /**
     * Create a new key in store
     *
     * @param keyId
     *          identifier for key
     *
     * @throws KeyStoreException
     *          throw when failed to access keystore
     */
    public void addNewKey( final String keyId ) throws KeyStoreException {
        final KeyStore keyStore = getKeyStore();
        try {
            final KeyGenerator keyGenerator = KeyGenerator.getInstance( keyGenerationAlgorithm, keystoreProvider );
            keyGenerator.init( new KeyGenParameterSpec.Builder( keyId, KeyProperties.PURPOSE_SIGN).setUserAuthenticationRequired(true).build() );
            keyGenerator.generateKey();
        } catch ( final Exception e ) {
            throw new KeyGenerationException( e );
        }
    }

    /**
     * Remove key from store
     *
     * @param keyId
     *          identifier for key
     *
     * @throws KeyStoreException
     *          throw when failed to access keystore
     */
    public void removeKey( final String keyId ) throws KeyStoreException {

        final KeyStore keyStore = getKeyStore();
        try {
            keyStore.deleteEntry( keyId );
        } catch( final Exception e ) {
            throw new KeyRemovalException( e );
        }
    }

    /**
     * Get key in store
     *
     * @param keyId
     *          identifier for key
     *
     * @return
     *          key to be requested
     *
     * @throws KeyStoreException
     *          throw when failed to access keystore
     */
    public SecretKey getKey( final String keyId ) throws KeyStoreException {
        final KeyStore keyStore = getKeyStore();
        try {
            return (SecretKey) keyStore.getKey(keyId, null);
        } catch ( final Exception e ) {
            throw new KeyAccessException( e );
        }
    }

}
