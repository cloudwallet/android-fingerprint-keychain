/*
 * Blocko Inc.
 * __________________
 *
 * (C) 2017 Blocko Inc. Some rights reserved. <tech@blocko.io>
 *
 * This file is part of Coinstack OpenKeyChain Android Fingerprint Authentication.
 *
 * Coinstack OpenKeyChain Android Fingerprint Authentication under MIT License
 *
 * See LICENSE file for details
 */
package io.blocko.coinstack.keychain.fingerprint.android.exception;

import android.hardware.fingerprint.FingerprintManager;

/**
 * Exception when trying to scan fingerprint in lockout status.
 *
 * The lockout can be occurred when 5 authentication failures continuously.
 *
 * It will be released after 30 seconds.
 *
 * See https://source.android.com/compatibility/android-cdd.pdf for more details
 */
public class FingerprintLockoutException extends FingerprintScanException {
    public FingerprintLockoutException() {
        super( FingerprintManager.FINGERPRINT_ERROR_LOCKOUT );
    }
}
