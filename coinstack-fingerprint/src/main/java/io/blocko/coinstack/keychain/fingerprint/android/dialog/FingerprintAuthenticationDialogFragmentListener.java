/*
 * Blocko Inc.
 * __________________
 *
 * (C) 2017 Blocko Inc. Some rights reserved. <tech@blocko.io>
 *
 * This file is part of Coinstack OpenKeyChain Android Fingerprint Authentication.
 *
 * Coinstack OpenKeyChain Android Fingerprint Authentication under MIT License
 *
 * See LICENSE file for details
 */
package io.blocko.coinstack.keychain.fingerprint.android.dialog;

/**
 * Listner for {@link FingerprintAuthenticationDialogFragment}
 */
public interface FingerprintAuthenticationDialogFragmentListener {

    /**
     * Callback method to notify to be created
     *
     * @param fragment
     *          caller to notify
     */
    void onCreate( FingerprintAuthenticationDialogFragment fragment );

    /**
     * Callback method to notify to be dismissed
     *
     * @param fragment
     *          caller to notify
     */
    void onDismiss( FingerprintAuthenticationDialogFragment fragment );
}
