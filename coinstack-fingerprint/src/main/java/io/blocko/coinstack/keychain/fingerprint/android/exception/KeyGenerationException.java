/*
 * Blocko Inc.
 * __________________
 *
 * (C) 2017 Blocko Inc. Some rights reserved. <tech@blocko.io>
 *
 * This file is part of Coinstack OpenKeyChain Android Fingerprint Authentication.
 *
 * Coinstack OpenKeyChain Android Fingerprint Authentication under MIT License
 *
 * See LICENSE file for details
 */
package io.blocko.coinstack.keychain.fingerprint.android.exception;

/**
 * Exception in being failed to create new key in keystore
 */
public class KeyGenerationException extends  KeyStoreException {
    public KeyGenerationException( final Throwable cause ) {
        super( "Failed to create key", cause );
    }
}
