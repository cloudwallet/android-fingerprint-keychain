/*
 * Blocko Inc.
 * __________________
 *
 * (C) 2017 Blocko Inc. Some rights reserved. <tech@blocko.io>
 *
 * This file is part of Coinstack OpenKeyChain Android Fingerprint Authentication.
 *
 * Coinstack OpenKeyChain Android Fingerprint Authentication under MIT License
 *
 * See LICENSE file for details
 */
package io.blocko.coinstack.keychain.fingerprint.android.util;

/**
 * Utilities for hex
 */
public class HexUtils {
    /**
     * convert byte array to readable hexa decimal string
     *
     * @param ba
     *          byte array to be converted
     * @return
     *          readable hexa string
     */
    public static String hexFrom( final byte[] ba ) {
        if (ba == null || ba.length == 0) {
            return null;
        }

        final StringBuilder sb = new StringBuilder( ba.length * 2 );
        for ( int i = 0 ; i < ba.length ; ++i ) {
            if ( ba[i] < 16 ) {
                sb.append( "0" );
            }
            sb.append( Integer.toHexString( ba[i] ) );
        }
        return sb.toString();
    }

}
