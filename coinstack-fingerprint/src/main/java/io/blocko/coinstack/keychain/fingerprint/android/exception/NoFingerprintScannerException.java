/*
 * Blocko Inc.
 * __________________
 *
 * (C) 2017 Blocko Inc. Some rights reserved. <tech@blocko.io>
 *
 * This file is part of Coinstack OpenKeyChain Android Fingerprint Authentication.
 *
 * Coinstack OpenKeyChain Android Fingerprint Authentication under MIT License
 *
 * See LICENSE file for details
 */
package io.blocko.coinstack.keychain.fingerprint.android.exception;

import android.hardware.fingerprint.FingerprintManager;

/**
 * Exception in no fingerprint scanner on device.
 */
public class NoFingerprintScannerException extends FingerprintScanException {
    public NoFingerprintScannerException() {
        super( FingerprintManager.FINGERPRINT_ERROR_HW_UNAVAILABLE );
    }
}
