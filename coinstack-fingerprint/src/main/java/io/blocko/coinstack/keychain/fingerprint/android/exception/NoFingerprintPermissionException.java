/*
 * Blocko Inc.
 * __________________
 *
 * (C) 2017 Blocko Inc. Some rights reserved. <tech@blocko.io>
 *
 * This file is part of Coinstack OpenKeyChain Android Fingerprint Authentication.
 *
 * Coinstack OpenKeyChain Android Fingerprint Authentication under MIT License
 *
 * See LICENSE file for details
 */
package io.blocko.coinstack.keychain.fingerprint.android.exception;

/**
 * Exception in no permission for fingerprint scanning
 *
 * Check &lt;uses-permission android:name="android.permission.USE_FINGERPRINT" /> in your AndroidManifest.xml.
 */
public class NoFingerprintPermissionException extends FingerprintScanException {
    public NoFingerprintPermissionException( final SecurityException e ) {
        super( e );
    }
}
